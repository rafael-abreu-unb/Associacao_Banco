package model;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Banco {

	public static void main(String[] args) {
		float saldoAtual;
		
		Transacao transacoes = new Transacao();
		ContaCorrente conta = new ContaCorrente(1000.0f);
		
		saldoAtual = conta.getSaldo();
		
		transacoes.realizarTransacoes(new Date(), conta, 100.0f, "Dispósito Inicial", Movimento.DEPOSITAR);
		transacoes.realizarTransacoes(new Date(), conta, 350.00f, "Pagar condominio", Movimento.SACAR);
		transacoes.realizarTransacoes(new Date(), conta, 50.00f, "Conta de Luz", Movimento.SACAR);
		transacoes.realizarTransacoes(new Date(), conta, 54.00f, "Conta de Celular", Movimento.SACAR);
		transacoes.realizarTransacoes(new Date(), conta, 100.00f, "Salário de Estagiário", Movimento.DEPOSITAR);
		
		System.out.println("Emissão de Extrato");
		System.out.println("----------------------------");
		System.out.println("Saldo Inicial: " + saldoAtual);
		System.out.println("----------------------------");
		System.out.println("Data \t   Valor \tTipo de Transação \t Saldo Acumulado \t Mensagem");
		for(Movimento movimento: transacoes.getMovimentos()) {
			System.out.print(new SimpleDateFormat("dd/MM/yyyy").format(movimento.getData()));
			System.out.print(" " + movimento.getValor());
			System.out.print(" " + (movimento.getOperacao() == Movimento.DEPOSITAR ? "\tDepóstio" : "\tSaque"));
			if(movimento.getOperacao() == Movimento.DEPOSITAR)
				saldoAtual = saldoAtual + movimento.getValor();
			else
				saldoAtual = saldoAtual - movimento.getValor();
			System.out.print(" " + saldoAtual);
			System.out.println(" " + movimento.getHistorico());
		}
		System.out.println("Saldo Final: " + conta.getSaldo());
	}

}












