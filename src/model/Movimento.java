package model;

import java.util.Date;

public class Movimento {
	private Date data;
	private ContaCorrente conta;
	private String historico;
	private float valor;
	private int operacao;
	
	public static final int SACAR = 1;
	public static final int DEPOSITAR = 2;
	
	public Movimento(Date data, ContaCorrente conta, String historico, float valor, int operacao) {
		this.data = data;
		this.conta = conta;
		this.historico = historico;
		this.valor = valor;
		this.operacao = operacao;
	}
	
	public boolean movimentar() {
		if(operacao == SACAR) {
			return conta.sacar(valor);
		}
		else if(operacao == DEPOSITAR) {
			conta.depositar(valor);
			return true;
		}
		else {
			System.out.println("Operção Inválida");
			return false;
		}
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public ContaCorrente getConta() {
		return conta;
	}

	public void setConta(ContaCorrente conta) {
		this.conta = conta;
	}

	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public float getValor() {
		return valor;
	}

	public void setValor(float valor) {
		this.valor = valor;
	}

	public int getOperacao() {
		return operacao;
	}

	public void setOperacao(int operacao) {
		this.operacao = operacao;
	}
	
	
}































